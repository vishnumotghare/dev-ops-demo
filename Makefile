all:
	$(MAKE) -C ntpclient
	$(MAKE) -C timer

clean:
	rm -rf ntpclient/*.o ntpclient/ntpclient
	rm -rf timer/*.o timer/timerfd
	
