#include <stdio.h>
#include <sys/timerfd.h>
#include <pthread.h>
#include <poll.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>

static void * timer_thread(void *data);
static pthread_t  thread_id;

int time_fd,time_set;

static void call_back()
{
	printf("Call Callback\n");
}

static void * timer_thread(void *data)
{
	struct pollfd ufds;
	int read_fds;
	int s;	
	uint64_t exp;
	printf("In timer thread\n");
	
	while (1) {

		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		pthread_testcancel();
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

		ufds.fd = time_fd;
		ufds.events = POLLIN;

		read_fds = poll(&ufds, 1, -1);

		if (read_fds <= 0) {
			continue;
		}
		
		if  (ufds.revents & POLLIN) {
                	
			s = read(ufds.fd, &exp, sizeof(uint64_t));

                	if (s != sizeof(uint64_t)) continue;

			call_back();
		}
	}
}

int main(int argc, char *argv[])
{
	int pthread,s;
	struct itimerspec new_value;

	printf("%d\n",argc);	
	if (argc == 1) {
		printf("%s interval\n",argv[0]);
		exit (1);
	}
	pthread = pthread_create(&thread_id,NULL,timer_thread,NULL);

	if (pthread == 0) {
		printf("Pthread created successfully\n");
	}

	time_fd = timerfd_create(CLOCK_REALTIME,0);

	if (time_fd < 0 ) {
		printf("Timefd creation failed\n");
	}	

	new_value.it_value.tv_sec = atoi(argv[1]);

	new_value.it_value.tv_nsec = 0;

	new_value.it_interval.tv_sec = atoi(argv[1]);
	new_value.it_interval.tv_nsec = 0;

	
	time_set = timerfd_settime(time_fd, 0, &new_value,NULL);
	if (time_set) {
		printf("timerfd_settime failed\n");
	}
	sleep(60);

	close(time_fd);
	s = pthread_cancel(thread_id);
	if (s == 0)
		printf("cancel error = %d\n",s);

	if (pthread_join(thread_id, NULL) == 0)
		printf("Thread terminate successfully\n");

	printf("exit\n");
	return 0;

}
